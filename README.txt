
v1.0
插件描述:
	基于mybatis generate框架的二次开发，封装成maven plugin
	在原来生成方法的基础上，增加了一些自定义方法，可选择性生成
	两种生成策略，EntityModelPlugin,TempletModelPlugin
		
		EntityModelPlugin :在原来的mapper.java和mapper.xml基础上增加了自定义方法
		
		TempletModelPlugin:基本的CURD和自定义方法,都封装在baseMapper中,生成的mapper.java继承BaseMapper
						        实体类继承BaseEntity
						        可配置是否生成service,继承自可自定义的AbstractService
						        可配置是否生成controller,继承自可自定义的BaseController
						        所有父类，建议根据实际的项目情况，自定义开发，com.fastdemo.generatetemplet包下的基础类只是一个范例
						        最好在本插件的基础上再次开发


使用说明:
pom 文件中添加
<plugin>
	<groupId>com.fastdemo</groupId>
	<artifactId>schema-plugin</artifactId>
	<version>0.0.1-SNAPSHOT</version>
	<configuration>
		<!-- mybatis generator配置文件路径,大部分的生成配置在该文件的properties标签对应的.properties，生成模式是xml中的plugin标签  -->
		<generatorConfig>/src/main/resources/generatorConfig.xml</generatorConfig>
	</configuration>
</plugin>

实际使用，在要生成代码的项目
run as maven build:  配置global schema-plugin:generateCode
单独配置global，不在install的生命周期中是因为代码生成通常是一次性的，不需要每次install都生成
					
					
					
同样的配置，如果多次生成，java类会把前面的代码覆盖，但是mapper.xml会增量添加

配置文件说明：

	配置文件放在 /src/main/resources/generatorConfig.xml
	需要手动修改  generatorConfig.properties 

	参数说明：
		jdbcJar：本机的jdbc驱动绝对路径（一定要改！！一定要改！！一定要改！！）
		connectionURL：数据库连接URL
		userId:数据库用户名
		password:数据库密码
		
		targetProject:需要生成代码的目标工程路径，和项目同级目录
		systemPackage:项目通用的包路径
		subSystem：模块包名(小写)
		tableName:数据库表名
		entityName:实体类名（首字母大写）
		
		${targetProject}/src/main/java/${systemPackage}/${模块名}/${subSystem}/${生成具体类}
		
		demo： 
		targetProject=purang-mall

		system=mall
		systemPackage=com.purang.mall
		subSystem=merchant
		tableName=merchant
		entityName=Merchant
		
		模块					路径
		controller		com.purang.mall.controller.merchant.MerchantController
		dao				com.purang.mall.dao.merchant.MerchantMapper
		entity			com.purang.mall.entity.merchant.Merchant
		service			com.purang.mall.service.merchant.MerchantService
						com.purang.mall.service.merchant.impl.MerchantServiceImpl
		
		xml				/src/main/resources/mapper/mall/merchant/MerchantMapper.xml
		
		
	baseEntity说明(TempletModelPlugin模式下)：
		建表时需要加上baseEntity中的字段，除了 start/length/orderColumn/orderDir
		
		`create_time`  varchar(17) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL COMMENT '创建时间' ,
		`update_time`  varchar(17) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL COMMENT '更新时间' ,
		`create_user`  bigint(32) NULL DEFAULT NULL COMMENT '创建人ID' ,
		`update_user`  bigint(32) NULL DEFAULT NULL COMMENT '更新人ID' ,
		`create_user_name`  varchar(128) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL COMMENT '创建人NAME' ,
		`update_user_name`  varchar(128) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL COMMENT '更新人NAME' ,
		`version`  int(11) NULL DEFAULT NULL COMMENT 'version' ,
		`is_delete`  int(11) NULL DEFAULT NULL COMMENT '删除标记 ',

	
		
		
		dataTable说明
		controller中的 queryEntityDataTablePage
		前台查询时需要将参数转成json
		DEMO:
		
		table = $('#tbgrid').DataTable({
      
        "ajax": {
            "url": "/test/v1/queryTestDataTablePage.htm",
            "type": "POST",
            "contentType": "application/json",
            "data": function (data) {

                var defaultValue = $('#defaultValue').val().trim();
                if (defaultValue != '') {
                    data.defaultValue = defaultValue;
                }
                
                var testdate = $('#testdate').val().trim();
                if (testdate != '') {
                    data.testdate = testdate;
                }
            	
            	data = JSON.stringify(data);  // 参数转json
            	return data;
            }
			   