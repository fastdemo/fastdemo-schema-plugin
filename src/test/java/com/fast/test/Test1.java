/**
 * 
 */
package com.fast.test;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpression;
import javax.xml.xpath.XPathFactory;

import org.w3c.dom.Document;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;

/**
 * 
 * @date 2018年7月20日
 * @author guxingchun
 */
public class Test1 {

	public static final String projectPath = "E:/_workspace/purang/";
	private static String generatorConfigPath = "E:/_workspace/purang/mytest/src/main/resources/generatorConfig.xml";

	/**
	 * 
	 * @date 2018年7月20日
	 * @author guxingchun
	 * @throws Exception
	 */
	public static void main(String[] args) throws Exception {
		DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
		System.out.println("-*/-/-/-*/-*");
		XPathFactory XPathFactory = javax.xml.xpath.XPathFactory.newInstance();
		factory.setNamespaceAware(true); // never forget this!
		DocumentBuilder builder = factory.newDocumentBuilder();
		Document doc = builder.parse(generatorConfigPath);
		XPath xpath = XPathFactory.newXPath();
		XPathExpression compile = xpath.compile("//generatorConfiguration//properties");
		Object result = compile.evaluate(doc, XPathConstants.NODE);
		
		if (result instanceof Node) {
			Node node = (Node) result;
			NamedNodeMap attributes = node.getAttributes();
			Node namedItem = attributes.getNamedItem("resource");
			String nodeValue = namedItem.getNodeValue();
			System.out.println(nodeValue);
		}

	}
}