package com.fastdemo.tool.mybatis.utils;

import java.io.File;
import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.mybatis.generator.api.MyBatisGenerator;
import org.mybatis.generator.config.Configuration;
import org.mybatis.generator.config.Context;
import org.mybatis.generator.config.JavaClientGeneratorConfiguration;
import org.mybatis.generator.config.JavaModelGeneratorConfiguration;
import org.mybatis.generator.config.SqlMapGeneratorConfiguration;
import org.mybatis.generator.config.xml.ConfigurationParser;
import org.mybatis.generator.exception.InvalidConfigurationException;
import org.mybatis.generator.exception.XMLParserException;
import org.mybatis.generator.internal.DefaultShellCallback;

public class CMDGeneratorUtil {

	
	public static void main(String[] args) {
		
//		String path = GeneratorUtil.class.getResource("").getPath();
		String workspacePath = args[0];
		String genConfig = args[1];

		System.out.println("workspacePath: " + workspacePath) ;
		System.out.println("genConfig: " + genConfig);
		if(StringUtils.isEmpty(workspacePath)) {
			throw new RuntimeException("workspacePath:" + workspacePath);
		}
		if(StringUtils.isEmpty(genConfig)) {
			throw new RuntimeException("genConfig:" + genConfig);
		}
		
		
		generatorCode(workspacePath, genConfig);
	}
	
	public static void generatorCode(String workspacePath, String genCfg) {
		List<String> warnings = new ArrayList<String>();
		boolean overwrite = true;
		// String workspace = "../../../../../";
//		String path = GeneratorUtil.class.getResource("").getPath();
		// System.out.println(GeneratorCode.class.getResource("").getPath());
//		String path = GeneratorUtil.class.getResource("").getPath();
		
		String endPath = "/src/main/java";
		// String mapperEndPath = "/src/main/resources";
		
		File configFile = new File(genCfg);
		//System.out.println("config file path:" + configFile.getPath());
		ConfigurationParser cp = new ConfigurationParser(warnings);
		Configuration config = null;
		try {
			config = cp.parseConfiguration(configFile);

			Context context = config.getContexts().get(0);

			JavaClientGeneratorConfiguration jcgc = context
					.getJavaClientGeneratorConfiguration();
			jcgc.setTargetProject(workspacePath + jcgc.getTargetProject()
					+ endPath);

			JavaModelGeneratorConfiguration jmgc = context
					.getJavaModelGeneratorConfiguration();
			jmgc.setTargetProject(workspacePath + jmgc.getTargetProject()
					+ endPath);

			SqlMapGeneratorConfiguration smgc = context
					.getSqlMapGeneratorConfiguration();
			smgc.setTargetProject(workspacePath + smgc.getTargetProject()
					+ endPath);

			for (String line : warnings) {
				System.out.println("warning:" + line);
			}
		} catch (IOException e) {
			e.printStackTrace();
		} catch (XMLParserException e) {
			e.printStackTrace();
		}

		DefaultShellCallback callback = new DefaultShellCallback(overwrite);
		MyBatisGenerator myBatisGenerator = null;
		try {
			myBatisGenerator = new MyBatisGenerator(config, callback, warnings);
		} catch (InvalidConfigurationException e) {
			e.printStackTrace();
		}
		try {
			myBatisGenerator.generate(null);
			for (String line : warnings) {
				System.out.println("warning:" + line);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}

}
