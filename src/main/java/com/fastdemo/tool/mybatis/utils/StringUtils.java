package com.fastdemo.tool.mybatis.utils;

public class StringUtils {

	public static String unCapitalize(String str) {
		String start = str.substring(0, 1);
		String end = str.substring(1, str.length());
		start = start.toLowerCase();
		return start + end;
	}
}
