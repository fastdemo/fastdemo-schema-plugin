package com.fastdemo.tool.mybatis.utils;

import java.io.File;
import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.mybatis.generator.api.MyBatisGenerator;
import org.mybatis.generator.config.Configuration;
import org.mybatis.generator.config.Context;
import org.mybatis.generator.config.JavaClientGeneratorConfiguration;
import org.mybatis.generator.config.JavaModelGeneratorConfiguration;
import org.mybatis.generator.config.SqlMapGeneratorConfiguration;
import org.mybatis.generator.config.xml.ConfigurationParser;
import org.mybatis.generator.exception.InvalidConfigurationException;
import org.mybatis.generator.exception.XMLParserException;
import org.mybatis.generator.internal.DefaultShellCallback;

public class GeneratorUtil {

	
	public static void main(String[] args) {
		
//		String path = GeneratorUtil.class.getResource("").getPath();
//		String workspacePath = path.substring(0, path.indexOf("purang-schema"));
//		String genCfg = workspacePath + "purang-schema/src/main/resources/generatorConfig.xml";
//
//		generatorCode(workspacePath, genCfg);
		
		
		String generateConfigRelativePath = "/src/main/resources/generatorConfig.xml";
		generatorCode(GeneratorUtil.class, generateConfigRelativePath);
		
	}
	
	public static void printfPath(Class<?> utilClass, String generateConfigRelativePath) {
		String path = utilClass.getResource("").getPath();
		String projectName = path.substring(path.indexOf("/"), path.lastIndexOf("/"));
		String relativelyPath = System.getProperty("user.dir"); 
		relativelyPath = relativelyPath.replace("\\", "/");
		System.out.println("relativelyPath" + relativelyPath);
		String[] split = relativelyPath.split("/");
//		for (String string : split) {
//			System.out.println(string);
//		}
		projectName = split[split.length -1];
		String workspacePath = relativelyPath.substring(0, relativelyPath.indexOf(projectName));
		
		String genCfg = workspacePath + projectName + generateConfigRelativePath;
//		String genCfg = workspacePath + projectName + "/src/main/resources/purangGeneratorConfig.xml";

		System.out.println("projectName=" + projectName);
		System.out.println("workspacePath=" + workspacePath);
		System.out.println("genCfg=" + genCfg);
	}
	
	/**
	 * @param mainClass	main函数所在类
	 * @param generateConfigRelativePath	purangGeneratorConfig.xml配置文件在项目中的相对路径,例：/src/main/resource/purangGeneratorConfig.xml
	 * @date 2018年1月26日
	 * @author guxingchun
	 */
	public static void generatorCode(Class<?> mainClass, String generateConfigRelativePath) {
		String path = mainClass.getResource("").getPath();
		String projectName = path.substring(path.indexOf("/"), path.lastIndexOf("/"));
		String relativelyPath = System.getProperty("user.dir"); 
		relativelyPath = relativelyPath.replace("\\", "/");
		System.out.println("relativelyPath" + relativelyPath);
		String[] split = relativelyPath.split("/");
		projectName = split[split.length -1];
		String workspacePath = relativelyPath.substring(0, relativelyPath.indexOf(projectName));
		
		String genCfg = workspacePath + projectName + generateConfigRelativePath;

		System.out.println("projectName=" + projectName);
		System.out.println("workspacePath=" + workspacePath);
		System.out.println("genCfg=" + genCfg);
		
		generatorCode(workspacePath, genCfg);
	}
	
	public static void generatorCode(String workspacePath, String genCfg) {
		List<String> warnings = new ArrayList<String>();
		boolean overwrite = true;
		// String workspace = "../../../../../";
//		String path = GeneratorUtil.class.getResource("").getPath();
		// System.out.println(GeneratorCode.class.getResource("").getPath());
//		String path = GeneratorUtil.class.getResource("").getPath();
		
		String endPath = "/src/main/java";
		String mapperEndPath = "/src/main/resources";
		
		File configFile = new File(genCfg);
		//System.out.println("config file path:" + configFile.getPath());
		ConfigurationParser cp = new ConfigurationParser(warnings);
		Configuration config = null;
		try {
			config = cp.parseConfiguration(configFile);

			Context context = config.getContexts().get(0);

			JavaClientGeneratorConfiguration jcgc = context
					.getJavaClientGeneratorConfiguration();
			jcgc.setTargetProject(workspacePath + jcgc.getTargetProject()
					+ endPath);

			JavaModelGeneratorConfiguration jmgc = context
					.getJavaModelGeneratorConfiguration();
			jmgc.setTargetProject(workspacePath + jmgc.getTargetProject()
					+ endPath);

			SqlMapGeneratorConfiguration smgc = context
					.getSqlMapGeneratorConfiguration();
			smgc.setTargetProject(workspacePath + smgc.getTargetProject()
					+ mapperEndPath);

			for (String line : warnings) {
				System.out.println("warning:" + line);
			}
		} catch (IOException e) {
			e.printStackTrace();
		} catch (XMLParserException e) {
			e.printStackTrace();
		}

		DefaultShellCallback callback = new DefaultShellCallback(overwrite);
		MyBatisGenerator myBatisGenerator = null;
		try {
			myBatisGenerator = new MyBatisGenerator(config, callback, warnings);
		} catch (InvalidConfigurationException e) {
			e.printStackTrace();
		}
		try {
			myBatisGenerator.generate(null);
			for (String line : warnings) {
				System.out.println("warning:" + line);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}

}
