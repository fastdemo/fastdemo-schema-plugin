package com.fastdemo.tool.mybatis.method.impl;

import java.util.List;

import org.mybatis.generator.api.IntrospectedColumn;
import org.mybatis.generator.api.IntrospectedTable;
import org.mybatis.generator.api.dom.java.Interface;
import org.mybatis.generator.api.dom.java.TopLevelClass;
import org.mybatis.generator.api.dom.xml.Attribute;
import org.mybatis.generator.api.dom.xml.Document;
import org.mybatis.generator.api.dom.xml.TextElement;
import org.mybatis.generator.api.dom.xml.XmlElement;
import org.mybatis.generator.config.Context;
import org.mybatis.generator.internal.types.JdbcTypeNameTranslator;

import com.fastdemo.tool.mybatis.method.GennerageMethod;
import com.fastdemo.tool.mybatis.method.MethonType;

public class UpdateBatchByIdGenerate extends AbstractClientGenerated {

	@Override
	public boolean generatedMethod(Interface interfaze, TopLevelClass topLevelClass, IntrospectedTable introspectedTable) {
		return true;
	}

	@Override
	public boolean generatedSql(Document document, IntrospectedTable introspectedTable, Context context) {
		XmlElement parentElement = document.getRootElement();

		context.getCommentGenerator().addComment(parentElement);
		XmlElement answer = new XmlElement(MethonType.UPDATE.getMethodTag()); //$NON-NLS-1$

		answer.addAttribute(new Attribute("id", GennerageMethod.UPDATE_BATCH_BY_ID.getMapperName())); //$NON-NLS-1$

		String parameterType = "java.util.List";

		answer.addAttribute(new Attribute("parameterType", //$NON-NLS-1$
				parameterType));

		StringBuilder sb = new StringBuilder();
		XmlElement foreach = new XmlElement("foreach");
		answer.addElement(foreach);

		sb.append("update "); //$NON-NLS-1$
		sb.append(introspectedTable.getAliasedFullyQualifiedTableNameAtRuntime());
		foreach.addElement(new TextElement(sb.toString()));

		foreach.addAttribute(new Attribute("collection", //$NON-NLS-1$
				"list"));
		foreach.addAttribute(new Attribute("index", //$NON-NLS-1$
				"index"));
		foreach.addAttribute(new Attribute("item", //$NON-NLS-1$
				"item"));
		foreach.addAttribute(new Attribute("open", //$NON-NLS-1$
				""));
		foreach.addAttribute(new Attribute("separator", //$NON-NLS-1$
				";"));
		foreach.addAttribute(new Attribute("close", //$NON-NLS-1$
				""));
		XmlElement set = new XmlElement("set");
		foreach.addElement(set);
		List<IntrospectedColumn> introspectedColumns = introspectedTable.getNonPrimaryKeyColumns();
		for (IntrospectedColumn introspectedColumn : introspectedColumns) {
			String beanPropertyName = introspectedColumn.getJavaProperty();
			String columnName = introspectedColumn.getActualColumnName();
			String jdbcType = JdbcTypeNameTranslator.getJdbcTypeName(introspectedColumn.getJdbcType());
			XmlElement ifElement = new XmlElement("if");
			Attribute ifAttribute = new Attribute("test", "item." + beanPropertyName + " != null");
			ifElement.addAttribute(ifAttribute);
			ifElement.addElement(new TextElement(columnName + " = #{item." + beanPropertyName + ",jdbcType=" + jdbcType + "},"));
			set.addElement(ifElement);
		}
		TextElement textElement = new TextElement("where ID = #{item.id,jdbcType=VARCHAR}");
		foreach.addElement(textElement);
		parentElement.addElement(answer);
		document.setRootElement(parentElement);
		return true;
	}

	/* (non-Javadoc)
	 * @see com.fastdemo.tool.mybatis.method.impl.AbstractClientGenerated#getMethodType()
	 */
	@Override
	public MethonType getMethodType() {
		return MethonType.UPDATE;
	}
	
}
