package com.fastdemo.tool.mybatis.method.impl;

import org.mybatis.generator.api.IntrospectedTable;
import org.mybatis.generator.api.dom.java.Interface;
import org.mybatis.generator.api.dom.java.TopLevelClass;
import org.mybatis.generator.api.dom.xml.Attribute;
import org.mybatis.generator.api.dom.xml.Document;
import org.mybatis.generator.api.dom.xml.TextElement;
import org.mybatis.generator.api.dom.xml.XmlElement;
import org.mybatis.generator.config.Context;

import com.fastdemo.tool.mybatis.method.GennerageMethod;
import com.fastdemo.tool.mybatis.method.MethonType;

public class DeleteBySelectiveGenerate extends AbstractClientGenerated {

	@Override
	public boolean generatedMethod(Interface interfaze,
			TopLevelClass topLevelClass, IntrospectedTable introspectedTable) {
		return true;
	}

	@Override
	public boolean generatedSql(Document document,
			IntrospectedTable introspectedTable, Context context) {
		XmlElement parentElement = document.getRootElement();
		
		context.getCommentGenerator().addComment(parentElement);
		XmlElement answer = new XmlElement(MethonType.DELETE.getMethodTag()); //$NON-NLS-1$

		answer.addAttribute(new Attribute("id", GennerageMethod.DELETE_BY_SELECTIVE.getMapperName())); //$NON-NLS-1$

		String parameterType = introspectedTable.getBaseRecordType();
//		String parameterType = "map";
		
		answer.addAttribute(new Attribute("parameterType", //$NON-NLS-1$
				parameterType));

		StringBuilder sb = new StringBuilder();
		sb.append("delete from "); //$NON-NLS-1$
		sb.append(introspectedTable
				.getAliasedFullyQualifiedTableNameAtRuntime());
		answer.addElement(new TextElement(sb.toString()));

		XmlElement where = buildWhereElement(introspectedTable,context);

		answer.addElement(where);
		parentElement.addElement(answer);
		document.setRootElement(parentElement);
		return true;
	}

	/* (non-Javadoc)
	 * @see com.fastdemo.tool.mybatis.method.impl.AbstractClientGenerated#getMethodType()
	 */
	@Override
	public MethonType getMethodType() {
		return MethonType.DELETE;
	}

}
