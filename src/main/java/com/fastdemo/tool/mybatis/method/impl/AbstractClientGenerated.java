package com.fastdemo.tool.mybatis.method.impl;

import java.util.List;

import org.mybatis.generator.api.IntrospectedColumn;
import org.mybatis.generator.api.IntrospectedTable;
import org.mybatis.generator.api.dom.xml.Attribute;
import org.mybatis.generator.api.dom.xml.TextElement;
import org.mybatis.generator.api.dom.xml.XmlElement;
import org.mybatis.generator.config.Context;
import org.mybatis.generator.internal.types.JdbcTypeNameTranslator;

import com.fastdemo.tool.mybatis.method.MethodGenerated;
import com.fastdemo.tool.mybatis.method.MethonType;

public abstract class AbstractClientGenerated implements MethodGenerated{
	
	abstract public MethonType getMethodType();
	
	protected XmlElement getBaseColumnListElement(
			IntrospectedTable introspectedTable) {
		XmlElement answer = new XmlElement("include"); //$NON-NLS-1$
		answer.addAttribute(new Attribute("refid", //$NON-NLS-1$
				introspectedTable.getBaseColumnListId()));
		return answer;
	}

	protected XmlElement buildWhereElement(IntrospectedTable introspectedTable,Context context) {
		List<IntrospectedColumn> introspectedColumns = introspectedTable
				.getNonPrimaryKeyColumns();
		XmlElement where = new XmlElement("where");
		// where.addElement(new TextElement("1=1"));
		String whereFlag = context.getProperty("where");
		XmlElement ifElement;
		Attribute ifAttribute;
		String beanPropertyName;
		String columnName;
		String jdbcType;
		for (IntrospectedColumn introspectedColumn : introspectedColumns) {
			if(!"true".equals(whereFlag)){
				String isSelect = introspectedColumn.getProperties().getProperty(
						"selectKey");
				if (isSelect == null || !isSelect.equals("true")) {
					continue;
				}
			}
			beanPropertyName = introspectedColumn.getJavaProperty();
			columnName = introspectedColumn.getActualColumnName();
			jdbcType = JdbcTypeNameTranslator
					.getJdbcTypeName(introspectedColumn.getJdbcType());
			ifElement = new XmlElement("if");
			ifAttribute = new Attribute("test", beanPropertyName
					+ " != null");
			ifElement.addAttribute(ifAttribute);
			ifElement.addElement(new TextElement("AND " + columnName + " = #{"
					+ beanPropertyName + ",jdbcType=" + jdbcType + "}"));
			where.addElement(ifElement);
		}
		return where;
	}

}
