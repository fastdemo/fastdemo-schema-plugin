package com.fastdemo.tool.mybatis.method.impl;

import java.util.Iterator;

import org.mybatis.generator.api.IntrospectedColumn;
import org.mybatis.generator.api.IntrospectedTable;
import org.mybatis.generator.api.dom.java.Interface;
import org.mybatis.generator.api.dom.java.TopLevelClass;
import org.mybatis.generator.api.dom.xml.Attribute;
import org.mybatis.generator.api.dom.xml.Document;
import org.mybatis.generator.api.dom.xml.TextElement;
import org.mybatis.generator.api.dom.xml.XmlElement;
import org.mybatis.generator.codegen.mybatis3.MyBatis3FormattingUtilities;
import org.mybatis.generator.config.Context;

import com.fastdemo.tool.mybatis.method.GennerageMethod;
import com.fastdemo.tool.mybatis.method.MethonType;

public class SelectByIdsGenerate extends AbstractClientGenerated {

	@Override
	public boolean generatedMethod(Interface interfaze, TopLevelClass topLevelClass, IntrospectedTable introspectedTable) {
		return true;
	}

	@Override
	public boolean generatedSql(Document document, IntrospectedTable introspectedTable, Context context) {
		XmlElement parentElement = document.getRootElement();

		context.getCommentGenerator().addComment(parentElement);
		XmlElement answer = new XmlElement(MethonType.SELECT.getMethodTag()); //$NON-NLS-1$

		answer.addAttribute(new Attribute("id", GennerageMethod.SELECT_BY_IDS.getMapperName())); //$NON-NLS-1$
		answer.addAttribute(new Attribute("resultMap", //$NON-NLS-1$
				introspectedTable.getBaseResultMapId()));

		String parameterType = "java.util.List";

		answer.addAttribute(new Attribute("parameterType", //$NON-NLS-1$
				parameterType));

		StringBuilder sb = new StringBuilder();
		sb.append("select "); //$NON-NLS-1$

		if (introspectedTable.hasBaseColumns()) {
			answer.addElement(new TextElement(sb.toString()));
			answer.addElement(getBaseColumnListElement(introspectedTable));
		} else {

			Iterator<IntrospectedColumn> iter = introspectedTable.getAllColumns().iterator();
			while (iter.hasNext()) {
				sb.append(MyBatis3FormattingUtilities.getSelectListPhrase(iter.next()));

				if (iter.hasNext()) {
					sb.append(", "); //$NON-NLS-1$
				}

				if (sb.length() > 80) {
					answer.addElement(new TextElement(sb.toString()));
					sb.setLength(0);
				}
			}
			answer.addElement(new TextElement(sb.toString()));
		}

		sb.setLength(0);
		sb.append(" from ");
		sb.append(introspectedTable.getAliasedFullyQualifiedTableNameAtRuntime());
		sb.append(" where id in ");
		answer.addElement(new TextElement(sb.toString()));
		XmlElement foreach = new XmlElement("foreach");
		answer.addElement(foreach);

		foreach.addAttribute(new Attribute("collection", //$NON-NLS-1$
				"list"));
		foreach.addAttribute(new Attribute("index", //$NON-NLS-1$
				"index"));
		foreach.addAttribute(new Attribute("item", //$NON-NLS-1$
				"item"));
		foreach.addAttribute(new Attribute("open", //$NON-NLS-1$
				"("));
		foreach.addAttribute(new Attribute("separator", //$NON-NLS-1$
				","));
		foreach.addAttribute(new Attribute("close", //$NON-NLS-1$
				")"));
		foreach.addElement(new TextElement("#{item}"));
		parentElement.addElement(answer);
		document.setRootElement(parentElement);
		return true;
	}

	/* (non-Javadoc)
	 * @see com.fastdemo.tool.mybatis.method.impl.AbstractClientGenerated#getMethodType()
	 */
	@Override
	public MethonType getMethodType() {
		return MethonType.SELECT;
	}
	
}
