package com.fastdemo.tool.mybatis.method;

import org.mybatis.generator.api.IntrospectedTable;
import org.mybatis.generator.api.dom.java.Interface;
import org.mybatis.generator.api.dom.java.TopLevelClass;
import org.mybatis.generator.api.dom.xml.Document;
import org.mybatis.generator.config.Context;

public interface MethodGenerated {

	public boolean generatedMethod(Interface interfaze,
			TopLevelClass topLevelClass, IntrospectedTable introspectedTable);
	
	public boolean generatedSql(Document document,
			IntrospectedTable introspectedTable,Context context);
	
}
