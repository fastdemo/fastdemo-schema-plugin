package com.fastdemo.tool.mybatis.method.impl;

import org.mybatis.generator.api.IntrospectedTable;
import org.mybatis.generator.api.dom.java.Interface;
import org.mybatis.generator.api.dom.java.TopLevelClass;
import org.mybatis.generator.api.dom.xml.Attribute;
import org.mybatis.generator.api.dom.xml.Document;
import org.mybatis.generator.api.dom.xml.TextElement;
import org.mybatis.generator.api.dom.xml.XmlElement;
import org.mybatis.generator.config.Context;

import com.fastdemo.tool.mybatis.method.GennerageMethod;
import com.fastdemo.tool.mybatis.method.MethonType;

public class DeleteBatchByIdGenerate extends AbstractClientGenerated {
	

	@Override
	public boolean generatedMethod(Interface interfaze, TopLevelClass topLevelClass, IntrospectedTable introspectedTable) {
		return true;
	}

	@Override
	public boolean generatedSql(Document document, IntrospectedTable introspectedTable, Context context) {
		XmlElement parentElement = document.getRootElement();

		context.getCommentGenerator().addComment(parentElement);
		XmlElement answer = new XmlElement(MethonType.DELETE.getMethodTag()); //$NON-NLS-1$

		answer.addAttribute(new Attribute("id", GennerageMethod.DELETE_BATCH_BY_ID.getMapperName())); //$NON-NLS-1$

		String parameterType = "java.util.List";

		answer.addAttribute(new Attribute("parameterType", //$NON-NLS-1$
				parameterType));

		StringBuilder sb = new StringBuilder();
		sb.append("delete from "); //$NON-NLS-1$
		sb.append(introspectedTable.getAliasedFullyQualifiedTableNameAtRuntime());
		sb.append(" where id in ");
		answer.addElement(new TextElement(sb.toString()));
		XmlElement foreach = new XmlElement("foreach");
		answer.addElement(foreach);

		foreach.addAttribute(new Attribute("collection", //$NON-NLS-1$
				"list"));
		foreach.addAttribute(new Attribute("index", //$NON-NLS-1$
				"index"));
		foreach.addAttribute(new Attribute("item", //$NON-NLS-1$
				"item"));
		foreach.addAttribute(new Attribute("open", //$NON-NLS-1$
				"("));
		foreach.addAttribute(new Attribute("separator", //$NON-NLS-1$
				","));
		foreach.addAttribute(new Attribute("close", //$NON-NLS-1$
				")"));
		foreach.addElement(new TextElement("#{item}"));
		parentElement.addElement(answer);
		document.setRootElement(parentElement);
		return true;
	}

	/* (non-Javadoc)
	 * @see com.fastdemo.tool.mybatis.method.impl.AbstractClientGenerated#getMethodType()
	 */
	@Override
	public MethonType getMethodType() {
		return MethonType.DELETE;
	}

}
