package com.fastdemo.tool.mybatis.plugin;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.mybatis.generator.api.GeneratedJavaFile;
import org.mybatis.generator.api.IntrospectedColumn;
import org.mybatis.generator.api.IntrospectedTable;
import org.mybatis.generator.api.Plugin;
import org.mybatis.generator.api.PluginAdapter;
import org.mybatis.generator.api.dom.java.Field;
import org.mybatis.generator.api.dom.java.FullyQualifiedJavaType;
import org.mybatis.generator.api.dom.java.Interface;
import org.mybatis.generator.api.dom.java.JavaVisibility;
import org.mybatis.generator.api.dom.java.Method;
import org.mybatis.generator.api.dom.java.Parameter;
import org.mybatis.generator.api.dom.java.TopLevelClass;
import org.mybatis.generator.api.dom.xml.Document;
import org.mybatis.generator.api.dom.xml.Element;
import org.mybatis.generator.api.dom.xml.TextElement;
import org.mybatis.generator.api.dom.xml.XmlElement;

import com.fastdemo.generate.GenerateConstant;
import com.fastdemo.tool.mybatis.java.JavaFileGenerated;
import com.fastdemo.tool.mybatis.method.GennerageMethod;
import com.fastdemo.tool.mybatis.method.MethodGenerated;
import com.fastdemo.tool.mybatis.method.MethonType;
import com.fastdemo.tool.mybatis.method.impl.AbstractClientGenerated;
import com.fastdemo.tool.mybatis.method.impl.DeleteBatchByIdGenerate;
import com.fastdemo.tool.mybatis.method.impl.DeleteBySelectiveGenerate;
import com.fastdemo.tool.mybatis.method.impl.InsertBatchGenerate;
import com.fastdemo.tool.mybatis.method.impl.SelectByIdsGenerate;
import com.fastdemo.tool.mybatis.method.impl.SelectBySelectiveGenerate;
import com.fastdemo.tool.mybatis.method.impl.SelectCountGenerate;
import com.fastdemo.tool.mybatis.method.impl.SelectIdBySelectiveGenerate;
import com.fastdemo.tool.mybatis.method.impl.UpdateBatchByIdGenerate;
import com.fastdemo.tool.mybatis.method.impl.UpdateByIdsGenerate;
import com.fastdemo.tool.mybatis.method.impl.UpdateBySelectiveGenerate;
import com.fastdemo.tool.mybatis.utils.StringUtils;

public class EntityModelPlugin extends PluginAdapter {

	private List<MethodGenerated> methodGenerateds = new ArrayList<MethodGenerated>();

	private List<JavaFileGenerated> javaFileGenerateds = new ArrayList<JavaFileGenerated>();

	private List<String> javaDocLines = new ArrayList<String>();

	private static final String version = "V1.0";

	private static boolean listParameter = false;

	public EntityModelPlugin() {
		super();

		String str = GenerateConstant.df.format(new Date());
		String currentUser = System.getProperty("user.name");
		javaDocLines.add("/**");
		javaDocLines.add(" * @Description auto create by schema-tool-" + version);
		javaDocLines.add(" * ");
		javaDocLines.add(" * @author " + currentUser);
		javaDocLines.add(" * @date " + str);
		javaDocLines.add(" * ");
		javaDocLines.add(" */");
	}

	@Override
	public boolean validate(List<String> arg0) {
		return true;
	}

	@Override
	public void initialized(IntrospectedTable introspectedTable) {

		// 确定哪些方法需要生成
		String property;
		for (GennerageMethod method : GennerageMethod.values()) {
			property = context.getProperty("generageMethod_" + method.getMapperName());
			if(GenerateConstant.SWITCH_ON.equals(property))
				method.setGenerate(true);
			if (method.isGenerate() && method.isListParameter())
				listParameter = true;
		}

		for (GennerageMethod method : GennerageMethod.values()) {
			if (method.isGenerate())
				methodGenerateds.add(method.getTargetClass());
		}
	}

	@Override
	public boolean modelFieldGenerated(Field field, TopLevelClass topLevelClass, IntrospectedColumn introspectedColumn,
			IntrospectedTable introspectedTable, Plugin.ModelClassType modelClassType) {
		return true;
	}

	@Override
	public boolean modelGetterMethodGenerated(Method method, TopLevelClass topLevelClass, IntrospectedColumn introspectedColumn,
			IntrospectedTable introspectedTable, Plugin.ModelClassType modelClassType) {
		return true;
	}

	@Override
	public boolean modelSetterMethodGenerated(Method method, TopLevelClass topLevelClass, IntrospectedColumn introspectedColumn,
			IntrospectedTable introspectedTable, Plugin.ModelClassType modelClassType) {
		return true;
	}

	@Override
	public boolean modelBaseRecordClassGenerated(TopLevelClass topLevelClass, IntrospectedTable introspectedTable) {

		for (String line : javaDocLines) {
			topLevelClass.addJavaDocLine(line);
		}
		return true;
	}

	@Override
	public List<GeneratedJavaFile> contextGenerateAdditionalJavaFiles(IntrospectedTable introspectedTable) {

		// GenerateBaseBean.baseBeanCopyToTarget(context.getProperty("generateBase"),
		// context.getProperty("systemPackage"),
		// context.getJavaClientGeneratorConfiguration().getTargetProject());
		List<GeneratedJavaFile> gjfs = new ArrayList<GeneratedJavaFile>();
		for (JavaFileGenerated javaFileGenerated : javaFileGenerateds) {
			List<GeneratedJavaFile> rgjfs = javaFileGenerated.contextGenerateAdditionalJavaFiles(introspectedTable, context, javaDocLines);
			if (rgjfs != null && rgjfs.size() > 0) {
				gjfs.addAll(rgjfs);
			}
		}
		return gjfs;
	}

	@Override
	public boolean clientGenerated(Interface interfaze, TopLevelClass topLevelClass, IntrospectedTable introspectedTable) {

		for (String line : javaDocLines) {
			interfaze.addJavaDocLine(line);
		}

		FullyQualifiedJavaType entityType;
		for (GennerageMethod gennerageMethod : GennerageMethod.values()) {
			if (!gennerageMethod.isGenerate())
				continue;
			entityType = introspectedTable.getRules().calculateAllFieldsClass();
			interfaze.addMethod(buildMethod(gennerageMethod, entityType));
		}

		if (listParameter) {
			interfaze.addImportedType(FullyQualifiedJavaType.getNewListInstance());
		}

		return true;
	}

	/**
	 * @param gennerageMethod
	 * @return
	 * @date 2018年7月25日
	 * @author guxingchun
	 */
	private Method buildMethod(GennerageMethod gennerageMethod, FullyQualifiedJavaType entityType) {
		Method method = new Method();
		method.setVisibility(JavaVisibility.PUBLIC);

		method.setName(gennerageMethod.getMapperName());

		FullyQualifiedJavaType returnType = null;

		AbstractClientGenerated targetClass = gennerageMethod.getTargetClass();
		MethonType methodType = targetClass.getMethodType();

		switch (methodType) {
		case DELETE:
		case UPDATE:
		case INSERT:
			returnType = FullyQualifiedJavaType.getIntInstance();
			break;

		case SELECT:
			returnType = FullyQualifiedJavaType.getNewListInstance();
			returnType.addTypeArgument(entityType);
			break;
		case SELECT_ONE:
			returnType = entityType;
			break;
		default:
			break;

		}
		method.setReturnType(returnType);

		List<Parameter> parameterList = new ArrayList<Parameter>();
		Parameter parameter = null;

		if (targetClass instanceof InsertBatchGenerate) {
			FullyQualifiedJavaType javaType = FullyQualifiedJavaType.getNewListInstance();
			javaType.addTypeArgument(entityType);
			parameter = new Parameter(javaType, StringUtils.unCapitalize(entityType.getShortName()));
			parameterList.add(parameter);
		} else if (targetClass instanceof DeleteBatchByIdGenerate) {
			FullyQualifiedJavaType javaType = FullyQualifiedJavaType.getNewListInstance();
			javaType.addTypeArgument(FullyQualifiedJavaType.getStringInstance());
			parameter = new Parameter(javaType, "ids");
			parameterList.add(parameter);
		} else if (targetClass instanceof DeleteBySelectiveGenerate) {
			parameter = new Parameter(entityType, StringUtils.unCapitalize(entityType.getShortName()));
			parameterList.add(parameter);
		} else if (targetClass instanceof UpdateBatchByIdGenerate) {
			FullyQualifiedJavaType javaType = FullyQualifiedJavaType.getNewListInstance();
			javaType.addTypeArgument(entityType);
			parameter = new Parameter(javaType, StringUtils.unCapitalize(entityType.getShortName()));
			parameterList.add(parameter);
		} else if (targetClass instanceof UpdateByIdsGenerate) {
			FullyQualifiedJavaType javaType = FullyQualifiedJavaType.getNewListInstance();
			javaType.addTypeArgument(FullyQualifiedJavaType.getStringInstance());
			parameter = new Parameter(javaType, "ids");
			parameterList.add(parameter);
			parameter = new Parameter(entityType, StringUtils.unCapitalize(entityType.getShortName()));
			parameterList.add(parameter);

		} else if (targetClass instanceof UpdateBySelectiveGenerate) {
			parameter = new Parameter(entityType, StringUtils.unCapitalize(entityType.getShortName()));
			parameterList.add(parameter);
		} else if (targetClass instanceof SelectByIdsGenerate) {

		} else if (targetClass instanceof SelectBySelectiveGenerate) {
			parameter = new Parameter(entityType, StringUtils.unCapitalize(entityType.getShortName()));
			parameterList.add(parameter);
		} else if (targetClass instanceof SelectCountGenerate) {
			parameter = new Parameter(entityType, StringUtils.unCapitalize(entityType.getShortName()));
			parameterList.add(parameter);
		} else if (targetClass instanceof SelectIdBySelectiveGenerate) {
			parameter = new Parameter(entityType, StringUtils.unCapitalize(entityType.getShortName()));
			parameterList.add(parameter);
		}

		if (parameterList != null && parameterList.size() > 0) {
			for (Parameter parameterTemp : parameterList) {
				method.addParameter(parameterTemp);
			}
		}

		// List<T> selectBySelective(T entity);

		return method;
	}

	@Override
	public boolean sqlMapDocumentGenerated(Document document, IntrospectedTable introspectedTable) {
		for (MethodGenerated clientGenerated : methodGenerateds) {
			clientGenerated.generatedSql(document, introspectedTable, context);
		}
		XmlElement parentElement = document.getRootElement();
		List<Element> oldElements = new ArrayList<Element>();
		oldElements.addAll(parentElement.getElements());
		parentElement.getElements().clear();
		parentElement.getElements().add(new TextElement(""));
		for (Element element : oldElements) {
			parentElement.getElements().add(element);
			parentElement.addElement(new TextElement(""));
		}
		return true;
	}

	@Override
	public boolean sqlMapInsertElementGenerated(XmlElement element, IntrospectedTable introspectedTable) {
		return super.sqlMapInsertElementGenerated(element, introspectedTable);
	}

	public boolean sqlMapSelectByPrimaryKeyElementGenerated(XmlElement element, IntrospectedTable introspectedTable) {
		return super.sqlMapSelectByPrimaryKeyElementGenerated(element, introspectedTable);
	}

	public boolean sqlMapDeleteByPrimaryKeyElementGenerated(XmlElement element, IntrospectedTable introspectedTable) {
		return super.sqlMapDeleteByPrimaryKeyElementGenerated(element, introspectedTable);
	}

	public boolean sqlMapUpdateByPrimaryKeySelectiveElementGenerated(XmlElement element, IntrospectedTable introspectedTable) {
		return super.sqlMapUpdateByPrimaryKeySelectiveElementGenerated(element, introspectedTable);
	}

	public boolean sqlMapUpdateByPrimaryKeyWithoutBLOBsElementGenerated(XmlElement element, IntrospectedTable introspectedTable) {
		return super.sqlMapUpdateByPrimaryKeyWithoutBLOBsElementGenerated(element, introspectedTable);
	}

}
