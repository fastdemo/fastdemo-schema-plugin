package com.fastdemo.tool.mybatis.java.impl;

import java.util.ArrayList;
import java.util.List;

import org.mybatis.generator.api.GeneratedJavaFile;
import org.mybatis.generator.api.IntrospectedTable;
import org.mybatis.generator.api.dom.java.Field;
import org.mybatis.generator.api.dom.java.FullyQualifiedJavaType;
import org.mybatis.generator.api.dom.java.Interface;
import org.mybatis.generator.api.dom.java.JavaVisibility;
import org.mybatis.generator.api.dom.java.Method;
import org.mybatis.generator.api.dom.java.TopLevelClass;
import org.mybatis.generator.config.Context;
import org.mybatis.generator.config.PropertyRegistry;

import com.fastdemo.tool.mybatis.java.JavaFileGenerated;
import com.fastdemo.tool.mybatis.utils.StringUtils;

public class ServiceGenerated implements JavaFileGenerated {

	@Override
	public List<GeneratedJavaFile> contextGenerateAdditionalJavaFiles(IntrospectedTable introspectedTable, Context context,
			List<String> javaDocLines) {
		List<GeneratedJavaFile> generatedJavaFiles = new ArrayList<GeneratedJavaFile>();
		GeneratedJavaFile interfaceJavaFile = GenerateInterface(
				introspectedTable, context, javaDocLines);
		generatedJavaFiles.add(interfaceJavaFile);
		GeneratedJavaFile implJavaFile = GenerateImpl(introspectedTable,
				context, javaDocLines, interfaceJavaFile.getCompilationUnit()
						.getType());
		generatedJavaFiles.add(implJavaFile);
		return generatedJavaFiles;
	}
	
	private GeneratedJavaFile GenerateInterface(IntrospectedTable introspectedTable, Context context, List<String> javaDocLines) {
		
		String serviceBasePackge = context.getProperty("servicePackage");
		String subSystem = context.getProperty("subSystem");
		String entityService = context.getProperty("entityService");
		String system = context.getProperty("system");
		if (org.apache.commons.lang.StringUtils.isEmpty(system)) {
			system = "";
		} else {
			system = "." + system;
		}
		serviceBasePackge = serviceBasePackge + system;
		String interfacePackage = serviceBasePackge + "." + subSystem;

		FullyQualifiedJavaType entityType = introspectedTable.getRules()
				.calculateAllFieldsClass();
		String serviceName = entityType.getShortName();
		
		Interface interfac = new Interface(interfacePackage + "."
				+ serviceName + "Service");
		
		interfac.setVisibility(JavaVisibility.PUBLIC);
		FullyQualifiedJavaType superType = new FullyQualifiedJavaType(entityService);
		superType.addTypeArgument(entityType);
		
		interfac.addSuperInterface(superType);
		interfac.addImportedType(superType);
		
		for (String line : javaDocLines) {
			interfac.addJavaDocLine(line);
		}
		
		GeneratedJavaFile service = new GeneratedJavaFile(
				interfac,
				context.getJavaModelGeneratorConfiguration().getTargetProject(),
				context.getProperty(PropertyRegistry.CONTEXT_JAVA_FILE_ENCODING),
				context.getJavaFormatter());
		return service;
	}

	private GeneratedJavaFile GenerateImpl(IntrospectedTable introspectedTable, Context context, List<String> javaDocLines,
			FullyQualifiedJavaType interfaceType) {
		String servicePackage = context.getProperty("servicePackage");
//		String mapperPackage = context.getProperty("mapperPackage");
		String daoPackage = context.getProperty("daoPackage");
		String subSystem = context.getProperty("subSystem");
		String system = context.getProperty("system");
		if (org.apache.commons.lang.StringUtils.isEmpty(system)) {
			system = "";
		} else {
			system = "." + system;
		}
		servicePackage = servicePackage + system;
		String abstractEntityService = context.getProperty("abstractEntityService");
		String baseMapper = context.getProperty("baseMapper");
		String annotationServiceClass = context
				.getProperty("annotationServiceClass");
		String annotationService = context.getProperty("annotationService");
		String annotationAutowiredClass = context
				.getProperty("annotationAutowiredClass");
		String annotationAutowired = context.getProperty("annotationAutowired");

		String implPackage = servicePackage + "." + subSystem;
		
		FullyQualifiedJavaType entityType = introspectedTable.getRules()
				.calculateAllFieldsClass();
		String entityName = entityType.getShortName();
		String serviceName = entityName + "ServiceImpl";

		
		TopLevelClass serviceImpl = new TopLevelClass(implPackage + "." + serviceName);
		
		serviceImpl.setVisibility(JavaVisibility.PUBLIC);
		FullyQualifiedJavaType superType = new FullyQualifiedJavaType(abstractEntityService);
		superType.addTypeArgument(entityType);
		
		serviceImpl.setSuperClass(superType);
		serviceImpl.addImportedType(superType);
		
		serviceImpl.addAnnotation(annotationService);
		serviceImpl.addImportedType(new FullyQualifiedJavaType(annotationServiceClass));
		
		serviceImpl.addSuperInterface(interfaceType);
		serviceImpl.addImportedType(interfaceType);
		
		String mapperName = entityName + "Mapper";
		FullyQualifiedJavaType mapperType = new FullyQualifiedJavaType(
				daoPackage + system + "." + subSystem + "." + mapperName);
		serviceImpl.addImportedType(mapperType);
		
		String logger = context.getProperty("logger");
		String loggerFactory = context.getProperty("loggerFactory");
		FullyQualifiedJavaType loggerFactoryType = new FullyQualifiedJavaType(loggerFactory);
		FullyQualifiedJavaType loggerType = new FullyQualifiedJavaType(logger);
		serviceImpl.addImportedType(loggerType);
		serviceImpl.addImportedType(loggerFactoryType);
		
		Field logField = new Field();
		logField.setVisibility(JavaVisibility.PRIVATE);
		logField.setStatic(true);
		logField.setFinal(true);
		logField.setType(loggerType);
		logField.setName("logger");
		
		logField.addJavaDocLine("");
		logField.addJavaDocLine("/**");
		logField.addJavaDocLine(" * " + interfaceType.getShortName() + " log");
		logField.addJavaDocLine(" */");
		logField.setInitializationString("LoggerFactory.getLogger("+serviceName+".class)");
		serviceImpl.getFields().add(0, logField);

		Field mapperField = new Field();
		mapperField.setVisibility(JavaVisibility.PRIVATE);
		mapperField.setStatic(false);
		mapperField.setFinal(false);
		mapperField.setType(mapperType);
		mapperField.setName(StringUtils.unCapitalize(mapperName));
		mapperField.addAnnotation(annotationAutowired);
		mapperField.addJavaDocLine("/**");
		mapperField.addJavaDocLine(" * " + entityName + " Mapper");
		mapperField.addJavaDocLine(" */");
		serviceImpl.getFields().add(1, mapperField);
		serviceImpl.addImportedType(new FullyQualifiedJavaType(
				annotationAutowiredClass));
		
		Method constructorMethod = new Method();
		constructorMethod.addJavaDocLine("/**");
		constructorMethod.addJavaDocLine(" * Default Constructor");
		constructorMethod.addJavaDocLine(" */");
		constructorMethod.setVisibility(JavaVisibility.PUBLIC);
		constructorMethod.setName(serviceName);
		constructorMethod.addBodyLine("super();");
		constructorMethod.addBodyLine("logger.debug(\""+serviceName+" init complete\");");
		constructorMethod.setConstructor(true);
		serviceImpl.addMethod(constructorMethod);
		
		Method getMapperMethod = new Method();
		getMapperMethod.setVisibility(JavaVisibility.PUBLIC);
		getMapperMethod.addJavaDocLine("/**");
		getMapperMethod.addJavaDocLine(" * get " + entityName + " Mapper");
		getMapperMethod.addJavaDocLine(" */");
		getMapperMethod.addAnnotation("@Override");
		getMapperMethod.setName("getMapper");
		
		FullyQualifiedJavaType baseMapperType = new FullyQualifiedJavaType(baseMapper);
		baseMapperType.addTypeArgument(entityType);
		serviceImpl.addImportedType(baseMapperType);
		
		getMapperMethod.setReturnType(baseMapperType);
		getMapperMethod.addBodyLine("return " + StringUtils.unCapitalize(mapperName) + ";");
		serviceImpl.addMethod(getMapperMethod);
		
		for (String line : javaDocLines) {
			serviceImpl.addJavaDocLine(line);
		}
		
		GeneratedJavaFile service = new GeneratedJavaFile(
				serviceImpl,
				context.getJavaModelGeneratorConfiguration().getTargetProject(),
				context.getProperty(PropertyRegistry.CONTEXT_JAVA_FILE_ENCODING),
				context.getJavaFormatter());
		return service;
	}
	
	
}
