package com.fastdemo.tool.mybatis.java.impl;

import java.util.ArrayList;
import java.util.List;

import org.mybatis.generator.api.GeneratedJavaFile;
import org.mybatis.generator.api.IntrospectedColumn;
import org.mybatis.generator.api.IntrospectedTable;
import org.mybatis.generator.api.dom.java.Field;
import org.mybatis.generator.api.dom.java.FullyQualifiedJavaType;
import org.mybatis.generator.api.dom.java.JavaVisibility;
import org.mybatis.generator.api.dom.java.Method;
import org.mybatis.generator.api.dom.java.Parameter;
import org.mybatis.generator.api.dom.java.TopLevelClass;
import org.mybatis.generator.config.Context;
import org.mybatis.generator.config.PropertyRegistry;
import org.mybatis.generator.internal.util.JavaBeansUtil;

import com.fastdemo.tool.mybatis.java.JavaFileGenerated;
import com.fastdemo.tool.mybatis.plugin.TempletModelPlugin;


public class DTOGenerated implements JavaFileGenerated {

	@Override
	public List<GeneratedJavaFile> contextGenerateAdditionalJavaFiles(IntrospectedTable introspectedTable, Context context,
			List<String> javaDocLines) {

		String dtoBasePackage = context.getProperty("dtoPackage");
		String subSystem = context.getProperty("subSystem");
		String baseDTO = context.getProperty("baseDTO");

		List<GeneratedJavaFile> generatedJavaFiles = new ArrayList<GeneratedJavaFile>();

		String dtoPackage = dtoBasePackage + "." + subSystem;

		FullyQualifiedJavaType entityType = introspectedTable.getRules().calculateAllFieldsClass();
		String dtoName = entityType.getShortName();

		TopLevelClass topLevelClass = new TopLevelClass(dtoPackage + "." + dtoName + "DTO");

		topLevelClass.setVisibility(JavaVisibility.PUBLIC);

		FullyQualifiedJavaType baseDTOTyp = new FullyQualifiedJavaType(baseDTO);

		topLevelClass.setSuperClass(baseDTOTyp);
		topLevelClass.addImportedType(baseDTOTyp);

		List<IntrospectedColumn> columns = introspectedTable.getNonPrimaryKeyColumns();

		Field javaBeansField;
		Method beanMethod;
		for (IntrospectedColumn introspectedColumn : columns) {

			if (TempletModelPlugin.oFieldList.contains(introspectedColumn.getJavaProperty()))
				continue;

			javaBeansField = getJavaBeansField(introspectedColumn);
			topLevelClass.addField(javaBeansField);
			topLevelClass.addImportedType(javaBeansField.getType());

			beanMethod = getJavaBeansGetter(introspectedColumn);
			topLevelClass.addMethod(beanMethod);

			beanMethod = getJavaBeansSetter(introspectedColumn);
			topLevelClass.addMethod(beanMethod);
		}

		Field serialVersionField = new Field();
		serialVersionField.setVisibility(JavaVisibility.PRIVATE);
		serialVersionField.setStatic(true);
		serialVersionField.setFinal(true);
		serialVersionField.setType(new FullyQualifiedJavaType("long"));
		serialVersionField.setName("serialVersionUID");
		serialVersionField.setInitializationString("1L");
		serialVersionField.addJavaDocLine("");
		serialVersionField.addJavaDocLine("/*");
		serialVersionField.addJavaDocLine(" * SerialVersion");
		serialVersionField.addJavaDocLine(" */");

		topLevelClass.getFields().add(0, serialVersionField);

		for (String line : javaDocLines) {
			topLevelClass.addJavaDocLine(line);
		}

		GeneratedJavaFile dto = new GeneratedJavaFile(topLevelClass, context.getJavaModelGeneratorConfiguration().getTargetProject(),
				context.getProperty(PropertyRegistry.CONTEXT_JAVA_FILE_ENCODING), context.getJavaFormatter());

		generatedJavaFiles.add(dto);
		return generatedJavaFiles;
	}

	private Field getJavaBeansField(IntrospectedColumn introspectedColumn) {
		FullyQualifiedJavaType javaType = introspectedColumn.getFullyQualifiedJavaType();
		String fieldComment = "COLUMN:" + introspectedColumn.getActualColumnName();
		if (introspectedColumn.getRemarks() != null && !introspectedColumn.getRemarks().equals("")) {
			fieldComment = introspectedColumn.getRemarks() + " " + fieldComment;
		}
		String property = introspectedColumn.getJavaProperty();
		Field field = new Field();
		field.setVisibility(JavaVisibility.PRIVATE);
		field.setType(javaType);
		field.setName(property);
		field.addJavaDocLine("/**");
		field.addJavaDocLine(" * " + fieldComment);
		field.addJavaDocLine(" */");
		return field;
	}

	private Method getJavaBeansGetter(IntrospectedColumn introspectedColumn) {
		FullyQualifiedJavaType javaType = introspectedColumn.getFullyQualifiedJavaType();
		String javaProperty = introspectedColumn.getJavaProperty();

		Method method = new Method();
		method.setVisibility(JavaVisibility.PUBLIC);
		method.setName(JavaBeansUtil.getGetterMethodName(javaProperty, javaType));
		method.setReturnType(javaType);

		StringBuilder sb = new StringBuilder();

		sb.append("return ");
		sb.append(javaProperty);
		sb.append(";");
		method.addBodyLine(sb.toString());

		return method;
	}

	private Method getJavaBeansSetter(IntrospectedColumn introspectedColumn) {
		FullyQualifiedJavaType javaType = introspectedColumn.getFullyQualifiedJavaType();
		String javaProperty = introspectedColumn.getJavaProperty();

		Method method = new Method();
		method.setVisibility(JavaVisibility.PUBLIC);
		method.setName(JavaBeansUtil.getSetterMethodName(javaProperty));
		method.addParameter(new Parameter(javaType, javaProperty));

		StringBuilder sb = new StringBuilder();

		sb.append("this.");
		sb.append(javaProperty);
		sb.append(" = ");
		sb.append(javaProperty);
		sb.append(";");
		method.addBodyLine(sb.toString());

		return method;
	}

}
