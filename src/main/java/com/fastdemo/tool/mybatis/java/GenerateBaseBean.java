package com.fastdemo.tool.mybatis.java;

import java.io.File;

import com.fastdemo.tool.mybatis.utils.FileCopyUtils;


public class GenerateBaseBean {

	public static void baseBeanCopyToTarget(String generateBase, String systemPackage, String targetProject) {

		if (!"true".equals(generateBase))
			return;

		/**
		 * relativelyPathE:/_workspace/purang/purang-jf-schema
		 * projectName=purang-jf-schema workspacePath=E:/_workspace/purang/
		 * genCfg=E:/_workspace/purang/purang-jf-schema/src/main/resources/generatorConfig.xml
		 * targetProject=E:/_workspace/purang/purang-jf-schema/src/main
		 */
		String sourceFolder = "/src/main/java";
		String projectPath = targetProject.replace(sourceFolder, "");
		// String utilPackage = targetProject + "/" + systemPackage + "/utils";

		// src/main/java
		
		String source = projectPath + "/src/main/tool/com/purang/framework";
		String target = targetProject + "/com/purang/framework";
		
		
		File dirNew = new File(target);
		if(!dirNew.exists())
			dirNew.mkdirs();
		else {
			dirNew.delete();
			FileCopyUtils.deleteFile(dirNew);
		}
			
		FileCopyUtils.directory(source, target);

	}

	public static void main(String[] args) {
		File file = new File("E:\\_workspace\\purang\\purang-jf-schema\\src\\main\\tool\\com\\purang\\framework");

		System.out.println(file.exists());
		System.out.println(file);
	}

}
