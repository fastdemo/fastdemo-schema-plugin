package com.fastdemo.tool.mybatis.java.impl;

import java.util.ArrayList;
import java.util.List;

import org.mybatis.generator.api.GeneratedJavaFile;
import org.mybatis.generator.api.IntrospectedColumn;
import org.mybatis.generator.api.IntrospectedTable;
import org.mybatis.generator.api.dom.java.Field;
import org.mybatis.generator.api.dom.java.FullyQualifiedJavaType;
import org.mybatis.generator.api.dom.java.InitializationBlock;
import org.mybatis.generator.api.dom.java.JavaVisibility;
import org.mybatis.generator.api.dom.java.Method;
import org.mybatis.generator.api.dom.java.Parameter;
import org.mybatis.generator.api.dom.java.TopLevelClass;
import org.mybatis.generator.config.Context;
import org.mybatis.generator.config.PropertyRegistry;

import com.fastdemo.tool.mybatis.java.JavaFileGenerated;
import com.fastdemo.tool.mybatis.utils.StringUtils;

public class ControllerGenerated implements JavaFileGenerated {

	private FullyQualifiedJavaType httpServletResponseType;
	private FullyQualifiedJavaType httpServletRequestType;
	private FullyQualifiedJavaType mapType;
	private String annotationRequestMapping;
	private FullyQualifiedJavaType entityType;
	private String annotationRequestBody;
	private String annotationResponseBody;
	// private String annotationValid;
	// private FullyQualifiedJavaType searchType;

	// private FullyQualifiedJavaType dataTableSearchType; // 普兰 datatable特殊DTO

	@Override
	public List<GeneratedJavaFile> contextGenerateAdditionalJavaFiles(IntrospectedTable introspectedTable, Context context, List<String> javaDocLines) {

		List<GeneratedJavaFile> generatedJavaFileList = new ArrayList<GeneratedJavaFile>();

		String servicePackage = context.getProperty("servicePackage");
		String controllerPackage = context.getProperty("controllerPackage");
		String baseController = context.getProperty("baseController");
		String subSystem = context.getProperty("subSystem");
		String system = context.getProperty("system");

		String annotationAutowiredClass = context.getProperty("annotationAutowiredClass");
		String annotationAutowired = context.getProperty("annotationAutowired");

		String annotationControllerClass = context.getProperty("annotationControllerClass");
		String annotationController = context.getProperty("annotationController");

		String annotationRequestMappingClass = context.getProperty("annotationRequestMappingClass");
		annotationRequestMapping = context.getProperty("annotationRequestMapping");

		// String resultUtils = context.getProperty("resultUtils");
		// String convertUtils = context.getProperty("convertUtils");

		entityType = introspectedTable.getRules().calculateAllFieldsClass();
		String entityName = entityType.getShortName();

		TopLevelClass controller = new TopLevelClass(controllerPackage + "." + subSystem + "." + entityName + "Controller");

		controller.addImportedType(entityType);
		FullyQualifiedJavaType mapType = new FullyQualifiedJavaType("java.util.Map");
		FullyQualifiedJavaType hashMapType = new FullyQualifiedJavaType("java.util.HashMap");
		controller.addImportedType(mapType);
		controller.addImportedType(hashMapType);
		controller.addImportedType(new FullyQualifiedJavaType("org.apache.commons.lang.StringUtils"));
		// controller.addImportedType(new FullyQualifiedJavaType(convertUtils));

		controller.setVisibility(JavaVisibility.PUBLIC);

		FullyQualifiedJavaType superType = new FullyQualifiedJavaType(baseController);

		controller.setSuperClass(superType);
		controller.addImportedType(superType);

		controller.addAnnotation(annotationController);
		controller.addImportedType(new FullyQualifiedJavaType(annotationControllerClass));

		controller.addAnnotation(annotationRequestMapping + "(\"/" + StringUtils.unCapitalize(entityName) + "\")");
		controller.addImportedType(new FullyQualifiedJavaType(annotationRequestMappingClass));

		String serviceName = entityName + "Service";
		if (org.apache.commons.lang.StringUtils.isEmpty(system)) {
			system = "";
		} else {
			system = "." + system;
		}
		FullyQualifiedJavaType serverType = new FullyQualifiedJavaType(servicePackage + system + "." + subSystem + "." + serviceName);

		Field serviceField = new Field();
		serviceField.setName(StringUtils.unCapitalize(serviceName));
		serviceField.setStatic(false);
		serviceField.setFinal(false);
		serviceField.setVisibility(JavaVisibility.PRIVATE);
		serviceField.setType(serverType);
		serviceField.addAnnotation(annotationAutowired);
		serviceField.addJavaDocLine("");
		serviceField.addJavaDocLine("/**");
		serviceField.addJavaDocLine(" * " + entityName + " Service");
		serviceField.addJavaDocLine(" */");

		controller.addImportedType(serverType);
		controller.addImportedType(new FullyQualifiedJavaType(annotationAutowiredClass));

		controller.getFields().add(0, serviceField);

		Field orderColumnMapField = new Field();
		orderColumnMapField.setVisibility(JavaVisibility.PRIVATE);
		orderColumnMapField.setFinal(true);
		orderColumnMapField.setStatic(true);
		mapType.addTypeArgument(new FullyQualifiedJavaType("java.lang.String"));
		mapType.addTypeArgument(new FullyQualifiedJavaType("java.lang.String"));
		orderColumnMapField.setType(mapType);
		orderColumnMapField.addJavaDocLine("");
		orderColumnMapField.addJavaDocLine("/**");
		orderColumnMapField.addJavaDocLine(" * order column map");
		orderColumnMapField.addJavaDocLine(" */");
		orderColumnMapField.setName("orderColumnMap");
		orderColumnMapField.setInitializationString("new HashMap<String, String>()");

		controller.getFields().add(0, orderColumnMapField);

		InitializationBlock initializationBlock = new InitializationBlock();
		initializationBlock.setStatic(true);
		List<IntrospectedColumn> allColumns = introspectedTable.getAllColumns();
		if (allColumns != null && allColumns.size() > 0) {
			int index = 0;
			for (IntrospectedColumn column : allColumns) {
				initializationBlock.addBodyLine("orderColumnMap.put(\"" + index + "\", \"" + column.getActualColumnName() + "\");");
				index++;
			}
		}
		controller.addInitializationBlock(initializationBlock);

		// private final static Map<String, String> orderColumnMap = new HashMap<String,
		// String>();

		// String entityPackage = context.getProperty("entityPackage");
		// entityType = new FullyQualifiedJavaType(entityPackage + "." + subSystem + "."
		// + entityName);
		controller.addImportedType(entityType);

		// 构造control的函数体，annocation通用
		buildCommonJavaType(context, controller);

		controller.addMethod(buildMethod(JavaFileGenerated.METHOD_ADD, controller, entityType, serviceName, context));
		// controller.addMethod(buildMethod(JavaFileGenerated.METHOD_ADD_LIST,
		// controller, entityType, serviceName, context));
		controller.addMethod(buildMethod(JavaFileGenerated.METHOD_DELETE, controller, entityType, serviceName, context));
		// controller.addMethod(buildMethod(JavaFileGenerated.METHOD_DELETE_LIST,
		// controller, entityType, serviceName, context));
		controller.addMethod(buildMethod(JavaFileGenerated.METHOD_UPDATE, controller, entityType, serviceName, context));
		controller.addMethod(buildMethod(JavaFileGenerated.METHOD_QUERY, controller, entityType, serviceName, context));
		controller.addMethod(buildMethod(JavaFileGenerated.METHOD_QUERY_LIST, controller, entityType, serviceName, context));
		// controller.addMethod(buildMethod(JavaFileGenerated.METHOD_QUERY_PAGE,
		// controller, entityType, serviceName, context));
		// controller.addMethod(buildMethod(JavaFileGenerated.METHOD_QUERY_COUNT,
		// controller, entityType, serviceName, context));
		controller.addMethod(buildMethod(JavaFileGenerated.METHOD_QUERY_DATATABLE, controller, entityType, serviceName, context));

		GeneratedJavaFile generatedJavaFile = new GeneratedJavaFile(controller, context.getJavaModelGeneratorConfiguration().getTargetProject(), context.getProperty(PropertyRegistry.CONTEXT_JAVA_FILE_ENCODING), context.getJavaFormatter());

		for (String line : javaDocLines) {
			controller.addJavaDocLine(line);
		}

		generatedJavaFileList.add(generatedJavaFile);

		return generatedJavaFileList;
	}

	private void buildCommonJavaType(Context context, TopLevelClass controller) {

		String httpServletRequest = context.getProperty("httpServletRequest");
		String httpServletResponse = context.getProperty("httpServletResponse");
		httpServletResponseType = new FullyQualifiedJavaType(httpServletResponse);
		controller.addImportedType(httpServletResponseType);

		httpServletRequestType = new FullyQualifiedJavaType(httpServletRequest);
		controller.addImportedType(httpServletRequestType);

		String annotationRequestBodyClass = context.getProperty("annotationRequestBodyClass");
		annotationRequestBody = context.getProperty("annotationRequestBody");

		annotationResponseBody = context.getProperty("annotationResponseBody");

		String annotationResponseBodyClass = context.getProperty("annotationResponseBodyClass");
		// String annotationResponseBody =
		// context.getProperty("annotationResponseBody");

		mapType = new FullyQualifiedJavaType("java.util.Map");
		FullyQualifiedJavaType stringType = new FullyQualifiedJavaType("java.lang.String");
		FullyQualifiedJavaType objectType = new FullyQualifiedJavaType("java.lang.Object");
		/*
		 * FullyQualifiedJavaType arraysType = new FullyQualifiedJavaType(
		 * "java.util.Arrays");
		 */

		mapType.addTypeArgument(stringType);
		mapType.addTypeArgument(objectType);
		controller.addImportedType(mapType);
		// topLevelClass.addImportedType(arraysType);

		FullyQualifiedJavaType annotationRequestBodyType = new FullyQualifiedJavaType(annotationRequestBodyClass);
		controller.addImportedType(annotationRequestBodyType);

		FullyQualifiedJavaType annotationResponseBodyType = new FullyQualifiedJavaType(annotationResponseBodyClass);
		controller.addImportedType(annotationResponseBodyType);

		/*
		 * String annotationRequestParamClass = context
		 * .getProperty("annotationRequestParamClass"); String annotationRequestParam =
		 * context .getProperty("annotationRequestParam");
		 * 
		 * FullyQualifiedJavaType annotationRequestParamType = new
		 * FullyQualifiedJavaType( annotationRequestParamClass);
		 * topLevelClass.addImportedType(annotationRequestParamType);
		 */

		// String annotationValidClass = context.getProperty("annotationValidClass");
		// annotationValid = context.getProperty("annotationValid");

		// FullyQualifiedJavaType annotationValidType = new
		// FullyQualifiedJavaType(annotationValidClass);
		// controller.addImportedType(annotationValidType);

		// String annotationSecurityClass =
		// context.getProperty("annotationSecurityClass");
		// FullyQualifiedJavaType annotationSecurityType = new
		// FullyQualifiedJavaType(annotationSecurityClass);
		// controller.addImportedType(annotationSecurityType);

		String annotationRequestMethodClass = context.getProperty("annotationRequestMethodClass");
		FullyQualifiedJavaType annotationRequestMethodType = new FullyQualifiedJavaType(annotationRequestMethodClass);
		controller.addImportedType(annotationRequestMethodType);

		String page = context.getProperty("page");
		// String search = context.getProperty("search");
		// String searchDTO = context.getProperty("search");
		// String dataTableSearch = context.getProperty("dataTableSearch");

		// FullyQualifiedJavaType pageType = new FullyQualifiedJavaType(page);
		// topLevelClass.addImportedType(pageType);
		FullyQualifiedJavaType pageType = new FullyQualifiedJavaType(page);
		pageType.addTypeArgument(entityType);

		// searchType = new FullyQualifiedJavaType(search);
		// controller.addImportedType(searchType);
		// searchType.addTypeArgument(entityType);
		//
		// dataTableSearchType = new FullyQualifiedJavaType(dataTableSearch);
		// dataTableSearchType.addTypeArgument(entityType);
		// controller.addImportedType(dataTableSearchType);

		FullyQualifiedJavaType paramType = FullyQualifiedJavaType.getNewListInstance();
		controller.addImportedType(paramType);
	}

	private Method buildMethod(String methodType, TopLevelClass topLevelClass, FullyQualifiedJavaType entityType, String serviceName, Context context) {

		String entityName = entityType.getShortName();

		Method method = new Method();
		method.setVisibility(JavaVisibility.PUBLIC);
		method.setReturnType(mapType);

		buildMethodNameAndParamter(method, methodType, entityName);

		// String annotationSecurity = context.getProperty("annotationSecurity");
		// method.addAnnotation(annotationSecurity + "(checkLogon = true)");

		method.addJavaDocLine("/**");
		method.addJavaDocLine(" * " + methodType + " " + entityName + " from http");
		method.addJavaDocLine(" */");

		method.addBodyLines(buildBodyLines(methodType, entityName, serviceName));

		return method;
	}

	public void buildMethodNameAndParamter(Method method, String methodType, String entityName) {
		String methodName = null;
		String annotationRequestMappingStr = null;
		String annotationRequestMappingStrPrefix = annotationRequestMapping + "(value = \"/";
		String annotationRequestMappingStrSuffix = ".htm\" , method = RequestMethod.POST)";

		if (JavaFileGenerated.METHOD_QUERY_PAGE.equals(methodType) || JavaFileGenerated.METHOD_QUERY_LIST.equals(methodType)) {
			methodName = JavaFileGenerated.METHOD_QUERY + entityName + methodType;
		} else if (JavaFileGenerated.METHOD_ADD_LIST.equals(methodType)) {
			methodName = JavaFileGenerated.METHOD_ADD + entityName + JavaFileGenerated.METHOD_QUERY_LIST;
		} else if (JavaFileGenerated.METHOD_DELETE_LIST.equals(methodType)) {
			methodName = JavaFileGenerated.METHOD_DELETE + entityName + "ByIdList";
		} else if (JavaFileGenerated.METHOD_DELETE.equals(methodType)) {
			methodName = JavaFileGenerated.METHOD_DELETE + entityName + "ById";
		} else if (JavaFileGenerated.METHOD_UPDATE.equals(methodType)) {
			methodName = JavaFileGenerated.METHOD_UPDATE + entityName + "ById";
		} else if (JavaFileGenerated.METHOD_QUERY.equals(methodType)) {
			methodName = JavaFileGenerated.METHOD_QUERY + entityName + "ById";
		} else if (JavaFileGenerated.METHOD_QUERY_DATATABLE.equals(methodType)) {
			methodName = JavaFileGenerated.METHOD_QUERY + entityName + methodType + JavaFileGenerated.METHOD_QUERY_PAGE;
		} else {
			methodName = methodType + entityName;
		}

		method.setName(methodName);
		annotationRequestMappingStr = annotationRequestMappingStrPrefix + methodName.toLowerCase() + annotationRequestMappingStrSuffix;
		method.addAnnotation(annotationRequestMappingStr);
		Parameter parameter = null;
		if (JavaFileGenerated.METHOD_QUERY.equals(methodType)) {
			parameter = new Parameter(entityType, StringUtils.unCapitalize(entityType.getShortName()));
		} else if (JavaFileGenerated.METHOD_DELETE.equals(methodType)) {
			parameter = new Parameter(entityType, StringUtils.unCapitalize(entityType.getShortName()));
		} else if (JavaFileGenerated.METHOD_ADD_LIST.equals(methodType)) {
			FullyQualifiedJavaType paramType = FullyQualifiedJavaType.getNewListInstance();
			paramType.addTypeArgument(entityType);
			parameter = new Parameter(paramType, StringUtils.unCapitalize(entityName) + "s");
		} else if (JavaFileGenerated.METHOD_DELETE_LIST.equals(methodType)) {
			FullyQualifiedJavaType paramType = FullyQualifiedJavaType.getNewListInstance();
			paramType.addTypeArgument(FullyQualifiedJavaType.getStringInstance());
			parameter = new Parameter(paramType, StringUtils.unCapitalize(entityName) + "Ids");
		} else if (JavaFileGenerated.METHOD_QUERY_PAGE.equals(methodType)) {
			// parameter = new Parameter(searchType, "search");
			// parameter.addAnnotation(annotationValid);
		} else if (JavaFileGenerated.METHOD_QUERY_LIST.equals(methodType)) {
			parameter = new Parameter(entityType, StringUtils.unCapitalize(entityType.getShortName()));
		} else if (JavaFileGenerated.METHOD_QUERY_COUNT.equals(methodType)) {
			parameter = new Parameter(entityType, StringUtils.unCapitalize(entityType.getShortName()));
		} else if (JavaFileGenerated.METHOD_QUERY_COUNT.equals(methodType)) {
			parameter = new Parameter(entityType, StringUtils.unCapitalize(entityType.getShortName()));
		} else if (JavaFileGenerated.METHOD_QUERY_DATATABLE.equals(methodType)) {
			// parameter = new Parameter(dataTableSearchType, "dataTableSearch");
			// parameter.addAnnotation(annotationValid);
			parameter = new Parameter(entityType, StringUtils.unCapitalize(entityType.getShortName()));
			method.addAnnotation(annotationResponseBody);
			parameter.addAnnotation(annotationRequestBody);
		} else {
			parameter = new Parameter(entityType, StringUtils.unCapitalize(entityType.getShortName()));
		}

		if (parameter != null) {
//			parameter.addAnnotation(annotationRequestBody);
			method.addParameter(parameter);
		}

		Parameter httpServletRequestParameter = new Parameter(httpServletRequestType, "request");
		method.addParameter(httpServletRequestParameter);

		Parameter httpServletResponseParameter = new Parameter(httpServletResponseType, "response");
		method.addParameter(httpServletResponseParameter);

	}

	private List<String> buildBodyLines(String methodType, String entityName, String serviceName) {
		List<String> bodyLines = new ArrayList<String>();

		if (JavaFileGenerated.METHOD_ADD.equals(methodType)) {
			// bodyLines.add(entityName + " " + StringUtils.unCapitalize(entityName) + " =
			// ConvertUtils.convertDTO2Entity("
			// + StringUtils.unCapitalize(dtoName) + ", " + entityName + ".class);");
			bodyLines.add(StringUtils.unCapitalize(serviceName) + ".insert" + "(" + StringUtils.unCapitalize(entityName) + ");");
			bodyLines.add("Map<String, Object> resultMap = new HashMap<>();");
			bodyLines.add("resultMap.put(RESULT_KEY_CODE, SUCCESS_CODE);");
			bodyLines.add("resultMap.put(RESULT_KEY_MESSAGE, SUCCESS_MESSAGE);");
			bodyLines.add("return resultMap;");
		} else if (JavaFileGenerated.METHOD_ADD_LIST.equals(methodType)) {
			// bodyLines.add("List<" + entityName + "> " +
			// StringUtils.unCapitalize(entityName) + "s =
			// ConvertUtils.convertDTOList2EntityList("
			// + StringUtils.unCapitalize(entityName) + "DTOs, " + entityName + ".class);");
			bodyLines.add(StringUtils.unCapitalize(serviceName) + ".save(" + StringUtils.unCapitalize(entityName) + "s);");
			bodyLines.add("return ResultUtils.createResult();");
		} else if (JavaFileGenerated.METHOD_DELETE.equals(methodType)) {
			bodyLines.add(StringUtils.unCapitalize(serviceName) + ".deleteByPrimaryKey" + "(" + StringUtils.unCapitalize(entityName) + ".getId());");
			bodyLines.add("Map<String, Object> resultMap = new HashMap<>();");
			bodyLines.add("resultMap.put(RESULT_KEY_CODE, SUCCESS_CODE);");
			bodyLines.add("resultMap.put(RESULT_KEY_MESSAGE, SUCCESS_CODE);");
			bodyLines.add("return resultMap;");
		} else if (JavaFileGenerated.METHOD_DELETE_LIST.equals(methodType)) {
			bodyLines.add(StringUtils.unCapitalize(serviceName) + ".delete" + "ByIds(" + StringUtils.unCapitalize(entityName) + "Ids);");
			bodyLines.add("return ResultUtils.createResult();");
		} else if (JavaFileGenerated.METHOD_UPDATE.equals(methodType)) {
			// bodyLines.add(entityName + " " + StringUtils.unCapitalize(entityName) + " =
			// ConvertUtils.convertDTO2Entity("
			// + StringUtils.unCapitalize(dtoName) + ", " + entityName + ".class);");
			bodyLines.add(StringUtils.unCapitalize(serviceName) + ".updateByPrimaryKeySelective" + "(" + StringUtils.unCapitalize(entityName) + ");");
			bodyLines.add("Map<String, Object> resultMap = new HashMap<>();");
			bodyLines.add("resultMap.put(RESULT_KEY_CODE, SUCCESS_CODE);");
			bodyLines.add("resultMap.put(RESULT_KEY_MESSAGE, SUCCESS_CODE);");
			bodyLines.add("return resultMap;");
		} else if (JavaFileGenerated.METHOD_QUERY.equals(methodType)) {
			bodyLines.add(StringUtils.unCapitalize(entityName) + " = " + StringUtils.unCapitalize(entityName) + "Service.selectByPrimaryKey(" + StringUtils.unCapitalize(entityName) + ".getId());");
			// bodyLines.add(dtoName + " r" + dtoName + " = ConvertUtils.convertEntity2DTO("
			// + StringUtils.unCapitalize(entityName) + ", "
			// + dtoName + ".class);");
			bodyLines.add("Map<String, Object> resultMap = new HashMap<>();");
			bodyLines.add("resultMap.put(RESULT_KEY_CODE, SUCCESS_CODE);");
			bodyLines.add("resultMap.put(RESULT_KEY_MESSAGE, SUCCESS_CODE);");
			bodyLines.add("resultMap.put(\"data\", " + StringUtils.unCapitalize(entityName) + ");");
			bodyLines.add("return resultMap;");
		} else if (JavaFileGenerated.METHOD_QUERY_LIST.equals(methodType)) {
			// bodyLines.add("Search<" + entityName + "> search =
			// ConvertUtils.convertSearchDTO2Search(searchDTO, " + entityName + ".class);");
			bodyLines.add("List<" + entityName + "> entityList = " + StringUtils.unCapitalize(entityName) + "Service.selectBySelective(" + StringUtils.unCapitalize(entityName) + ");");
			bodyLines.add("Map<String, Object> resultMap = new HashMap<>();");
			bodyLines.add("resultMap.put(RESULT_KEY_CODE, SUCCESS_CODE);");
			bodyLines.add("resultMap.put(RESULT_KEY_MESSAGE, SUCCESS_CODE);");
			bodyLines.add("resultMap.put(\"data\", entityList);");
			bodyLines.add("return resultMap;");
		} else if (JavaFileGenerated.METHOD_QUERY_PAGE.equals(methodType)) {
			// bodyLines.add("Search<" + entityName + "> search =
			// ConvertUtils.convertSearchDTO2Search(searchDTO, " + entityName + ".class);");
			bodyLines.add("search = " + StringUtils.unCapitalize(entityName) + "Service.selectPageBySelective(search);");
			bodyLines.add("Map<String, Object> resultMap = new HashMap<>();");
			bodyLines.add("resultMap.put(RESULT_KEY_CODE, SUCCESS_CODE);");
			bodyLines.add("resultMap.put(RESULT_KEY_MESSAGE, SUCCESS_CODE);");
			bodyLines.add("resultMap.put(\"data\", search);");
			bodyLines.add("return resultMap;");
		} else if (JavaFileGenerated.METHOD_QUERY_COUNT.equals(methodType)) {
			// bodyLines.add(entityName + " " + StringUtils.unCapitalize(entityName) + " =
			// ConvertUtils.convertDTO2Entity("
			// + StringUtils.unCapitalize(dtoName) + ", " + entityName + ".class);");
			// Integer count = caseInfoService.queryCountByPara(caseInfo);
			bodyLines.add("Integer count = " + StringUtils.unCapitalize(entityName) + "Service.countBySelective(" + StringUtils.unCapitalize(entityName) + ");");
			bodyLines.add("Map<String, Object> resultMap = new HashMap<>();");
			bodyLines.add("resultMap.put(RESULT_KEY_CODE, SUCCESS_CODE);");
			bodyLines.add("resultMap.put(RESULT_KEY_MESSAGE, SUCCESS_CODE);");
			bodyLines.add("Map<String, Object> dataMap = new HashMap<>();");
			bodyLines.add("dataMap.put(\"count\", count);");
			bodyLines.add("resultMap.put(\"data\", dataMap);");
			bodyLines.add("return resultMap;");
		} else if (JavaFileGenerated.METHOD_QUERY_DATATABLE.equals(methodType)) {

			bodyLines.add("String orderColumn = request.getParameter(\"order[0][column]\");");
			bodyLines.add("String orderDir = request.getParameter(\"order[0][dir]\");");
			bodyLines.add("if (StringUtils.isNotBlank(orderColumn)) {");
			bodyLines.add(StringUtils.unCapitalize(entityName) + ".setOrderColumn(orderColumnMap.get(orderColumn));");
			bodyLines.add("}");
			bodyLines.add("if (StringUtils.isNotBlank(orderDir)) {");
			bodyLines.add(StringUtils.unCapitalize(entityName) + ".setOrderDir(orderDir);");
			bodyLines.add("}");
			bodyLines.add("");

			bodyLines.add("Integer totalCount = " + StringUtils.unCapitalize(entityName) + "Service.selectCountBySelective(" + StringUtils.unCapitalize(entityName) + ");");
			bodyLines.add("List<" + entityName + "> entitys = " + StringUtils.unCapitalize(entityName) + "Service.selectBySelective(" + StringUtils.unCapitalize(entityName) + ");");
			bodyLines.add("Map<String, Object> resultMap = new HashMap<>();");
			bodyLines.add("resultMap.put(RESULT_KEY_CODE, SUCCESS_CODE);");
			bodyLines.add("resultMap.put(RESULT_KEY_MESSAGE, SUCCESS_CODE);");
			bodyLines.add("resultMap.put(\"data\", entitys);");
			bodyLines.add("resultMap.put(\"iTotalDisplayRecords\", totalCount);");
			bodyLines.add("resultMap.put(\"iTotalRecords\", totalCount);");
			bodyLines.add("return resultMap;");
		}
		return bodyLines;
	}

}
