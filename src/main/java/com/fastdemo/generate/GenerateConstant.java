/**
 * 
 */
package com.fastdemo.generate;

import java.text.SimpleDateFormat;

/**
 * 
 * @date 2018年7月25日
 * @author guxingchun
 */
public class GenerateConstant {
	
	/**
	 * 开关开启
	 */
	public static final String SWITCH_ON = "true";
	
	public static final SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

	public static final String DEFAULT_ENTITY_TARGET_PROJECT = "-entity";
	
	public static final String DEFAULT_DTO_TARGET_PROJECT = "-dto";
	
	public static final String DEFAULT_MAPPER_TARGET_PROJECT = "-mapper";
	
	public static final String DEFAULT_MVC_TARGET_PROJECT = "-mvc";
	
	public static final String DEFAULT_SERVICE_TARGET_PROJECT = "-service";
	
	
	
}
