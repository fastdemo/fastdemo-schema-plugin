/**
 * 
 */
package com.fastdemo.generate;

import java.io.File;
import java.net.URL;
import java.util.HashMap;
import java.util.Map;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpression;
import javax.xml.xpath.XPathFactory;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.w3c.dom.Document;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;

/**
 * 
 * @date 2018年4月28日
 * @author guxingchun
 */
public class ConfigClassLoader extends ClassLoader {
	
	private final Logger logger = LoggerFactory.getLogger(ConfigClassLoader.class);

	private String projectPath;

	private String generatorConfigPath;

	private static final String SOURCE_FOLDER = "/src/main/resources/";

	// generage配置文件中property的节点路径
	private static final String GENERATOR_CONFIGURATION_XML_PATH = "//generatorConfiguration//properties";

	// generage配置文件中property的节点属性
	private static final String GENERATOR_CONFIGURATION_NODE_PROPERTY = "resource";

	private Map<String, URL> resourcesMap = new HashMap<>();

	public ConfigClassLoader(String workspacePath, String generatorConfig) {
		this.projectPath = workspacePath;
		this.generatorConfigPath = generatorConfig;

		URL configURL = null;

		DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
		try {

			XPathFactory XPathFactory = javax.xml.xpath.XPathFactory.newInstance();
			factory.setNamespaceAware(true);
			DocumentBuilder builder = factory.newDocumentBuilder();
			Document doc = builder.parse(this.projectPath + this.generatorConfigPath);
			XPath xpath = XPathFactory.newXPath();
			XPathExpression compile = xpath.compile(GENERATOR_CONFIGURATION_XML_PATH);
			Object result = compile.evaluate(doc, XPathConstants.NODE);
			
			if(result == null)
				logger.error("not find properties node in xml :" + this.projectPath + this.generatorConfigPath);
				

			String path;
			String propertyName = null;
			if (result instanceof Node) {
				Node node = (Node) result;
				NamedNodeMap attributes = node.getAttributes();
				Node namedItem = attributes.getNamedItem(GENERATOR_CONFIGURATION_NODE_PROPERTY);
				propertyName = namedItem.getNodeValue();
				logger.info("propertyName:" + propertyName);

				if (propertyName != null) {
					path = this.projectPath + SOURCE_FOLDER + propertyName;
					System.out.println(path);
					File propertyFile = new File(path);
					System.out.println(propertyFile.exists());
					logger.info("propertyFile exists:" + propertyFile.exists());
					if (propertyFile.exists()) {
						configURL = new URL("file:///" + path);
						resourcesMap.put(propertyName, configURL);
					}
				}
			}

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Override
	public URL getResource(String name) {
		URL configURL = null;

		configURL = super.getResource(name);
		if (configURL == null)
			configURL = resourcesMap.get(name);
		return configURL;
	}
}
