package com.fastdemo.generatetemplet.service;

import java.lang.reflect.ParameterizedType;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.transaction.annotation.Transactional;

import com.purang.mall.dao.BaseMapper;
import com.purang.mall.entity.BaseEntity;
import com.purang.mall.util.DateUtil;
import com.purang.mall.util.JsonUtils;
import com.purang.mall.util.ThreadDataUtil;

/**
 * 服务层基类<br>
 * <br>
 * 封装基础操作,增删改查等,具体实现类中可以通过覆盖方式扩展具体实现方法
 *
 * @param <T>
 */
public abstract class AbstractEntityService<T extends BaseEntity> implements BaseService<T> {

	protected Class<T> clazz;

	@SuppressWarnings("unchecked")
	public AbstractEntityService() {
		ParameterizedType type = (ParameterizedType) this.getClass().getGenericSuperclass();
		clazz = (Class<T>) type.getActualTypeArguments()[0];
	}

	public Class<T> getEntityType() {
		return clazz;
	}

	public String getEntitySimpleName() {
		return clazz.getSimpleName();
	}

	/**
	 * AbstractEntityService log
	 */

	private static final Logger logger = LogManager.getLogger(AbstractEntityService.class);
	// private static final Logger logger =
	// LoggerFactory.getLogger(AbstractEntityService.class);

	/**
	 * sub class must implements this method
	 * 
	 * @return
	 */
	public abstract BaseMapper<T> getMapper();

	/**
	 * 新增
	 * 
	 */
	@Override
	@Transactional
	public int insert(T entity) {
		logger.log(Level.DEBUG, "insert {0} : {1} ", clazz.getSimpleName(), entityToSerialString(entity));
		if (entity != null) {
			if (entity.getCreateTime() == null)
				entity.setCreateTime(DateUtil.getTimestampStr());
			if (entity.getCreateUser() == null)
				entity.setCreateUser(getUserIdFromThreadData());
			if (entity.getCreateUserName() == null)
				entity.setCreateUserName(getUserRealNameFromThreadData());
		}
		return getMapper().insert(entity);
	}

	/**
	 * 新增或更新
	 */
	@Override
	@Transactional
	public int saveOrUpdate(T entity) {
		logger.log(Level.DEBUG, "saveOrUpdate {0} : {1} ", clazz.getSimpleName(), entityToSerialString(entity));
		String id = entity.getId();
		String date = DateUtil.getTimestampStr();
		if (StringUtils.isBlank(id)) {
			if (entity.getCreateTime() == null)
				entity.setCreateTime(DateUtil.getTimestampStr());
			if (entity.getCreateUser() == null)
				entity.setCreateUser(getUserIdFromThreadData());
			if (entity.getCreateUserName() == null)
				entity.setCreateUserName(getUserRealNameFromThreadData());
			return this.insert(entity);
		} else {
			if (entity.getUpdateTime() == null)
				entity.setUpdateTime(date);
			if (entity.getUpdateUser() == null)
				entity.setUpdateUser(getUserIdFromThreadData());
			if (entity.getUpdateUserName() == null)
				entity.setUpdateUserName(getUserNameFromThreadData());
			return this.updateByPrimaryKeySelective(entity);
		}
	}

	/**
	 * delete by id 物理删除
	 */
	@Override
	@Transactional
	public int deleteByPrimaryKey(String id) {
		logger.log(Level.DEBUG, "realDeleteById {0} : {1} ", clazz.getSimpleName(), id);
		return getMapper().deleteByPrimaryKey(id);
	}

	/**
	 * 条件删除 物理删除
	 * 
	 */
	@Override
	@Transactional
	public int deleteBySelective(T entity) {
		logger.log(Level.DEBUG, "deleteBySelective {0} : {1} ", clazz.getSimpleName(), entityToSerialString(entity));
		return getMapper().deleteBySelective(entity);
	}

	/**
	 * 更新
	 * 
	 */
	@Transactional
	@Override
	public int updateByPrimaryKeySelective(T entity) {
		logger.log(Level.DEBUG, "updateByPrimaryKeySelective {0} : {1} ", clazz.getSimpleName(), entityToSerialString(entity));
		if (entity != null) {
			entity.setUpdateTime(DateUtil.getTimestampStr());
			entity.setUpdateUser(getUserIdFromThreadData());
			entity.setUpdateUserName(getUserNameFromThreadData());
		}
		return getMapper().updateByPrimaryKeySelective(entity);
	}

	/**
	 * select by id
	 */
	@Override
	public T selectByPrimaryKey(String id) {
		logger.log(Level.DEBUG, "selectByPrimaryKey {0} : {1} ", clazz.getSimpleName(), id);
		return getMapper().selectByPrimaryKey(id);
	}

	/**
	 * select by para
	 */
	public List<T> selectBySelective(T entity) {
		logger.log(Level.DEBUG, "selectBySelective {0} : {1} ", clazz.getSimpleName(), JsonUtils.beanToJson(entity));
		return getMapper().selectBySelective(entity);
	}

	/**
	 * select count by para
	 */
	@Override
	public Integer selectCountBySelective(T entity) {
		logger.log(Level.DEBUG, "updateByPrimaryKeySelective {0} : {1} ", clazz.getSimpleName(), entityToSerialString(entity));
		return getMapper().selectCountBySelective(entity);
	}

	/**
	 * entity to serialString
	 * 
	 * @param entity
	 * @return
	 */
	public String entityToSerialString(T entity) {
		if (entity != null) {
			return entity.toSerialString();
		}
		return null;
	}

	public String getUserIdFromThreadData() {
		return ThreadDataUtil.getUserId();
	}

	public String getUserNameFromThreadData() {
		return ThreadDataUtil.getUserName();
	}

	public String getUserRealNameFromThreadData() {
		return ThreadDataUtil.getUserRealName();
	}

	public String getUserMobileFromThreadData() {
		return ThreadDataUtil.getUserMobile();
	}
}
